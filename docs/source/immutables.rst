====================================================================
 :mod:`xotless.immutables` -- Utilities to work with immutable data
====================================================================

.. module:: xotless.immutables

.. autoclass:: ImmutableWrapper

.. autoclass:: ImmutableChainMap
