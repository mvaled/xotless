===================================================================
 :mod:`xotless.context` -- Extentions to `xotl.tools.context`:mod:
===================================================================

.. module:: xotless.context

.. testsetup::

   from xotless.context import *

.. autofunction:: ReentrantContext
