.. xotless documentation master file, created by
   sphinx-quickstart on Wed Apr 29 13:51:00 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xotless's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ranges
   types
   domains
   trees
   itertools
   pickablenv
   immutables
   walk
   context
   tracing
   testing

   history



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
