==================================================================
 :mod:`xotless.itertools` -- Iterables and iterable related tools
==================================================================

.. module:: xotless.itertools

.. testsetup:: *

   from xotless.itertools import *


.. autofunction:: tail

.. autofunction:: last


.. autoclass:: Line

.. autoclass:: CyclicLine
